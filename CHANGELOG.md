## [0.3.1](https://gitlab.com/zyehex/eslint-config-zyehex/compare/v0.3.0...v0.3.1) (2020-04-03)

### Bug Fixes

- **rules:** enable trailing comma ([a2dba06](https://gitlab.com/zyehex/eslint-config-zyehex/commit/a2dba0632bb365b96cefe5731e34653b8a72af09))

# [0.3.0](https://gitlab.com/zyehex/eslint-config-zyehex/compare/v0.2.3...v0.3.0) (2020-04-03)

### Bug Fixes

- remove proptypes rule ([cb8c3a5](https://gitlab.com/zyehex/eslint-config-zyehex/commit/cb8c3a5c2f5c352d3ba24cedc797e90f876499f1))

### Features

- add base rules ([c23d975](https://gitlab.com/zyehex/eslint-config-zyehex/commit/c23d97511cf460dcd0f1518ded7c0ff5be48b1c1))
- add cypress ([11b720d](https://gitlab.com/zyehex/eslint-config-zyehex/commit/11b720dfc60a211c94ac096340c7c8c05a7d6216))
- add jest rules ([428e9ec](https://gitlab.com/zyehex/eslint-config-zyehex/commit/428e9ec58585557ec0f6276f6f8140034ef7d28f))
- enable sort-keys ([7a27cb0](https://gitlab.com/zyehex/eslint-config-zyehex/commit/7a27cb04a73a5c143ed090033f3199b15121d314))

## [0.2.3](https://gitlab.com/zyehex/eslint-config-zyehex/compare/v0.2.2...v0.2.3) (2020-01-24)

### Bug Fixes

- allow implicit return type for expressions ([7ff7578](https://gitlab.com/zyehex/eslint-config-zyehex/commit/7ff75789f84d8a834c9d6a8b6e23f4b7bae415ee))
- disable default props rule ([d4f11d6](https://gitlab.com/zyehex/eslint-config-zyehex/commit/d4f11d6345506edee8cfa6ffe164248c724d5460))

## [0.2.1](https://gitlab.com/zyehex/eslint-config-zyehex/compare/v0.2.0...v0.2.1) (2020-01-20)

### Bug Fixes

- ts extension resolution ([01f1b7d](https://gitlab.com/zyehex/eslint-config-zyehex/commit/01f1b7dafb6b0fff22782393d74b27b503663a9a))

# [0.2.0](https://gitlab.com/zyehex/eslint-config-zyehex/compare/v0.1.2...v0.2.0) (2019-12-19)

### Features

- add react config ([9a1128f](https://gitlab.com/zyehex/eslint-config-zyehex/commit/9a1128fc41934faa77b3c3ab77de81f171081444))

## [0.1.2](https://gitlab.com/zyehex/eslint-config-zyehex/compare/v0.1.1...v0.1.2) (2019-12-17)

### Bug Fixes

- false positives on typescript method overloading ([48eaa0e](https://gitlab.com/zyehex/eslint-config-zyehex/commit/48eaa0e9e7cb6bf37646a6cd81cd289354e1946a))

## [0.1.1](https://gitlab.com/zyehex/eslint-config-zyehex/compare/v0.1.0...v0.1.1) (2019-12-01)

### Features

- remove consistent return ([db80e36](https://gitlab.com/zyehex/eslint-config-zyehex/commit/db80e36077a2ac9ba1e8a69d56e6b286c80853b9))

# [0.1.0](https://gitlab.com/zyehex/eslint-config-zyehex/compare/7179cb95f99b12d63bc2f50687464a7eab97dbd8...v0.1.0) (2019-11-13)

### Features

- add lint rules ([7179cb9](https://gitlab.com/zyehex/eslint-config-zyehex/commit/7179cb95f99b12d63bc2f50687464a7eab97dbd8))
