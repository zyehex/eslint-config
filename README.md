# eslint-config-zyehex

## Installation

```bash
# with yarn
yarn add -D eslint eslint-config-zyehex

# or if you prefer npm
npm i -D eslint eslint-config-zyehex
```

## Usage

Once the `eslint-config-zyehex` package is installed, you can use it by [extending](http://eslint.org/docs/user-guide/configuring#extending-configuration-files) it in your [ESLint configuration file](http://eslint.org/docs/user-guide/configuring).

```json
{
  "extends": "zyehex"
}
```
